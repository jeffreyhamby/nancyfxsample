﻿using Nancy;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Management;

namespace NancyService
{
    public class Dashboard : NancyModule
    {
        public Dashboard()
        {

            Get["/"] = parameters =>
            {
                return "<h1>NancyFx Test Service</h1>";
            };

            Get["/CPU"] = parameters =>
            {
                List<CpuUsage> cpuUsages = new List<CpuUsage>();

                ManagementObjectSearcher searcher =
                  new ManagementObjectSearcher("select * from Win32_PerfFormattedData_PerfOS_Processor");
                foreach (ManagementObject obj in searcher.Get())
                {
                    CpuUsage cpuUsage = new CpuUsage();
                    cpuUsage.PercentProcessorTime = obj["PercentProcessorTime"].ToString();
                    cpuUsage.Name = obj["Name"].ToString();
                    cpuUsages.Add(cpuUsage);
                }

                return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(cpuUsages);
            };

            Get["/ram"] = parameters =>
            {
                ObjectQuery wql = new ObjectQuery("SELECT * FROM Win32_OperatingSystem");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(wql);
                ManagementObjectCollection results = searcher.Get();

                List<RamUsage> ramUsages = new List<RamUsage>();
                foreach (ManagementObject result in results)
                {
                    RamUsage ramUsage = new RamUsage();
                    ramUsage.TotalVisibleMemory = result["TotalVisibleMemorySize"].ToString();
                    ramUsage.FreePhysicalMemory = result["FreePhysicalMemory"].ToString();
                    ramUsage.TotalVirtualMemory = result["TotalVirtualMemorySize"].ToString();
                    ramUsage.TotalVisibleMemory = result["TotalVisibleMemorySize"].ToString();
                    ramUsages.Add(ramUsage);
                }

                return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(ramUsages);
            };

            Get["/test/{procname}"] = parameters => //test with usp_Equipment_list
            {
                SqlConnection con = new SqlConnection("Server=localhost;Database=database;User Id=id;Password=pw;");
                SqlCommand cmd = new SqlCommand(parameters.procname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                sqlDa.Fill(dt);
                string test = JsonConvert.SerializeObject(dt);

                return test;
            };

        }

        public class CpuUsage
        {
            public string Name { get; set; }

            public string PercentProcessorTime { get; set; }
        }

        public class RamUsage
        {
            public string TotalVisibleMemory { get; set; }

            public string FreePhysicalMemory { get; set; }

            public string TotalVirtualMemory { get; set; }

            public string FreeVirtualMemory { get; set; }
        }
    }
}